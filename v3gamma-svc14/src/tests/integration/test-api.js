/* eslint-env mocha */
'use strict'

require('dotenv').config()

const chai = require('chai')
const expect = chai.expect
const chaiHttp = require('chai-http')
const url = require('url')
chai.use(chaiHttp)

describe('API Security', function () {
  this.timeout(10000)

  // get a new JWT before each test
  beforeEach(async () => {
    this.jwtToken = null

    // console.log(process.env.CLIENT_ID_2);
    /* eslint-disable-next-line */
    const oauth_url = url.parse(process.env.OAUTH_URL, true)

    const result = await chai
      .request(`${oauth_url.protocol}//${oauth_url.host}`)
      .post(oauth_url.path)
      .auth(process.env.CLIENT_ID, process.env.SECRET_KEY)
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .type('application/x-www-form-urlencoded')
      .send({
        grant_type: 'client_credentials',
        scope: 'bts-v3gamma-dev-v3gamma-svc14/title.write'
      })

    // save the token for the test
    this.jwtToken = result.body
  })

  it('calls to the secure API gateway work with Authentication', async () => {
    /* eslint-disable-next-line */
    const myUrl = url.parse(process.env.SERVICE_URL_SECURE, true)

    const result = await chai
      .request(`${myUrl.protocol}//${myUrl.host}`)
      .get(myUrl.path)
      .set('Authorization', `Bearer ${this.jwtToken.access_token}`)

    expect(result.statusCode).to.equal(200)
    expect(result.body).to.be.an('object')
    expect(result.body.message).to.be.a('string')
    expect(result.body.message).to.be.eq('Hello from Cognito secured lambda')
  })
})
