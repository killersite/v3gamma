'use strict'

exports.handler = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Hello from Cognito secured lambda'
    })
  }
}
