# v3gamma-svc14

API Service secured by Cognito OAuth. 

## Overview

This repo sets up an API Gateway resource backed by a Lambda and sets up a Cognito Authorizer on that resource.

## Technical Stack

- Node.js
- AWS SAM CLI >= 0.43.0

## Dependencies

- This assumes that a Cognito User Pool has been created.
- And the `Pool Id` has been added to SSM under name `/${Appgroup}/${Environment}/cognito/user_pool_id`

## Resources provisioned as part of SAM deployment

- Lambda Function (AWS::Serverless::Function)
- API Gateway (AWS::Serverless::Api)
- Resource Server & Scopes (AWS::Cognito::UserPoolResourceServer)

## Environment Variables

- None

## Caveat

You will find in the `template.yaml` some setup for the `ApiGatewayLoggingRole`. Please read the comments there.  
This is a one time process. As long as you have this enabled once in a region, you can deploy other stacks.

## Post Deploy

- You need to add the newly created Scopes to the "Allowed Custom Scopes" of the App Client.


## Gitlab Pipeline Description

The pipeline is run for the following Repo actions

- Merge Requests
- Feature branch checkin
- Master branch changes

The following Job are run

- install
- test
  - run code lint test
  - run unit tests
  - run integration tests
  - do security checks
- package
- deploy