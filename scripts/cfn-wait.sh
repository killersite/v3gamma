cloudformation_tail() {
  local stack="$1"
  local lastEvent
  local lastEventId
  local stackStatus=(`aws cloudformation describe-stacks --stack-name $stack --query Stacks[0].StackStatus --output text`)

  until \
	[ "$stackStatus" = "CREATE_COMPLETE" ] \
	|| [ "$stackStatus" = "CREATE_FAILED" ] \
	|| [ "$stackStatus" = "DELETE_COMPLETE" ] \
	|| [ "$stackStatus" = "DELETE_FAILED" ] \
	|| [ "$stackStatus" = "ROLLBACK_COMPLETE" ] \
	|| [ "$stackStatus" = "ROLLBACK_FAILED" ] \
	|| [ "$stackStatus" = "UPDATE_COMPLETE" ] \
	|| [ "$stackStatus" = "UPDATE_ROLLBACK_COMPLETE" ] \
	|| [ "$stackStatus" = "UPDATE_ROLLBACK_FAILED" ] \
	|| [ -z "$stackStatus" ]; do
	
  # Log all the cloudformation events
	eventId=(`aws cloudformation describe-stack-events --stack $stack --query StackEvents[0].PhysicalResourceId --output text`)
	if [ "$eventId" != "$lastEventId" ]
	then
		echo "Deploying/updating: $eventId"
    lastEventId=$eventId
	fi
	sleep 3
	stackStatus=(`aws cloudformation describe-stacks --stack-name $stack --query Stacks[0].StackStatus --output text`)
  done
  echo "Stack Status: $stackStatus"

  # Log all the Outputs
	stackOutputs=(`aws cloudformation describe-stacks --stack-name $stack --query Stacks[].Outputs[].OutputValue --output text`)
  count=0
  for i in ${stackOutputs[@]}; do
    echo -e "${stackOutputs[count]} \n"
    count=$(( $count + 1 ))
  done

  # Update the Environment URL
  DYNAMIC_ENVIRONMENT_URL=(`aws cloudformation describe-stacks --stack-name $stack --query "Stacks[0].Outputs[?OutputKey=='DirectApiURL'].OutputValue" --output text`)
  echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" > deploy.env

  if [ "$stackStatus" != "CREATE_COMPLETE" ] && [ "$stackStatus" != "UPDATE_COMPLETE" ] && [ "$stackStatus" != "DELETE_COMPLETE" ] && [ ! -z "$stackStatus" ]; then
    exit 1;
  fi
}

SERVICE_STACK_NAME=$1
cloudformation_tail $SERVICE_STACK_NAME 